$(document).ready( function () {

    $('.btn-login').click( function (event) {
        if ( $('#user_email').val() == '') {
            event.preventDefault();
            $('#user_email').removeClass('user_email');
            $('#user_email').addClass('invalid');
            $('#desc_email').removeClass('d-none');
        }

        if ( $('#user_password').val() == '') {
            event.preventDefault();
            $('#user_password').removeClass('user_password');
            $('#user_password').addClass('invalid');
            $('#desc_pass').removeClass('d-none');
        }
    })
});