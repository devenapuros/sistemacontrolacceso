$(document).ready(function () {

    id_materia_edit = '';
    id_materia_delete = '';

    tabla_materia = $('#dataTable').DataTable({
        "language":{
          "url": "//cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json"
        }
    });

    $('#create-btn').click(function (event) {
        event.preventDefault();
        let id_materia = $('#new_id_materia').val();
        let nombre = $('#new_materia_nombre').val();
        $.ajax({
            type: "POST",
            url: "crear-materia.php",
            data: {
                id_materia,
                nombre
            },
            success: function (data) {
                let json_data = JSON.parse(data);
                if (json_data.status == 202 ) {
                    $('#newModal').modal('hide');
                    $('#new_id_materia').val('');
                    $('#new_materia_nombre').val('');
                    tabla_materia.row.add( $(`
                    <tr>
                        <td>${id_materia}</td>
                        <td>${nombre}</td>
                        <td class="">
                            <button type="button" class="btn btn-outline-dark open-edit-modal ml-auto mr-1">
                                <ion-icon name="list-outline" style="font-size: 22px; vertical-align: middle;"></ion-icon>
                            </button>
                            <button type="button" class="btn btn-outline-danger open-delete-modal mr-auto ml-1">
                                <ion-icon name="trash-outline" style="font-size: 22px; vertical-align: middle;"></ion-icon>
                            </button>
                        </td>
                    </tr>
                    `)[0]).draw();
                    toastr["success"](json_data.message);
                }
                else if (json_data.status == 404 ) {
                    $('#newModal').modal('hide');
                    toastr["error"](json_data.message);
                }
            }
        });
    });

    $('#table-body').on('click','.open-edit-modal', function(event) {
        id_materia_edit = $(this).parent().parent().children()[0].innerHTML;
        $.ajax({
            type: "POST",
            url: "edit-materia-get.php",
            data: {
                id_materia: id_materia_edit
            },
            success: function (data) {
                let json_data = JSON.parse(data);
                $('#edit_id_materia').val(json_data.id_materia);
                $('#edit_materia_nombre').val(json_data.nombre);
            }
        });
        $('#editModal').modal('show');
    });


    $('#edit-btn').click(function (event) {
        let id_materia = $('#edit_id_materia').val();
        let nombre = $('#edit_materia_nombre').val();
        $('#editModal').modal('hide');
        $.ajax({
            type: "POST",
            url: "edit-materia-update.php",
            data: {
                id_materia,
                nombre
            },
            success: function (data) {
                let json_data = JSON.parse(data);
                if(json_data.status == 202)
                {
                    tabla_materia.destroy();
                    $('#table-body').html('');
                    json_materias = JSON.parse(json_data.materias)
                    json_materias.forEach(element => {
                        $('#table-body').append(`
                        <tr>
                            <td>${element.id_materia}</td>
                            <td>${element.nombre}</td>
                            <td class="">
                                <button type="button" class="btn btn-outline-dark open-edit-modal ml-auto mr-1">
                                    <ion-icon name="list-outline" style="font-size: 22px; vertical-align: middle;"></ion-icon>
                                </button>
                                <button type="button" class="btn btn-outline-danger open-delete-modal mr-auto ml-1">
                                    <ion-icon name="trash-outline" style="font-size: 22px; vertical-align: middle;"></ion-icon>
                                </button>
                            </td>
                        </tr>
                        `);
                    });
                    tabla_materia = $('#dataTable').DataTable({
                        "language":{"url":"//cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json"}
                    });
                    toastr["success"](json_data.message);
                }
                else if (json_data.status == 404 ) {
                    toastr["error"](json_data.message);
                }
            }
        });
    });

    $('#table-body').on('click','.open-delete-modal', function(event) {
        $('#deleteModal').modal('show');
        id_materia_delete = $(this).parent().parent().children()[0].innerHTML;
    });

    $('#delete-btn').click( function(event) {
        $('#deleteModal').modal('hide');
        $.ajax({
            type: "POST",
            url: "delete-materia.php",
            data: {
                id_materia: id_materia_delete
            },
            success: function (data) {
                let json_data = JSON.parse(data);
                if(json_data.status == 202){
                    tabla_materia.destroy();
                    $('#table-body').html('');
                    json_materias = JSON.parse(json_data.materias)
                    json_materias.forEach(element => {
                        $('#table-body').append(`
                        <tr>
                            <td>${element.id_materia}</td>
                            <td>${element.nombre}</td>
                            <td class="">
                                <button type="button" class="btn btn-outline-dark open-edit-modal ml-auto mr-1">
                                    <ion-icon name="list-outline" style="font-size: 22px; vertical-align: middle;"></ion-icon>
                                </button>
                                <button type="button" class="btn btn-outline-danger open-delete-modal mr-auto ml-1">
                                    <ion-icon name="trash-outline" style="font-size: 22px; vertical-align: middle;"></ion-icon>
                                </button>
                            </td>
                        </tr>
                        `);
                    });
                    tabla_materia = $('#dataTable').DataTable({
                        "language":{"url":"//cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json"}
                    });
                    toastr["success"](json_data.message);
                }
                else if (json_data.status == 404 ) {
                    toastr["error"](json_data.message);
                }
            }
        });
    });
});

toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": true,
    "positionClass": "toast-bottom-left",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "8000",
    "hideDuration": "1000",
    "timeOut": "8000",
    "extendedTimeOut": "6000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}