$(document).ready(function () {
    
    $('#search-btn').click( function(event) {
        event.preventDefault();
        let to_search = $('#to_search').val();
        //let cp_interno = $('#cp-interno').val();
        if (to_search != '') {

            if($('#to_search').hasClass('invalid'))
            {
                $('#to_search').removeClass('invalid');
                $('#to_search').addClass('to_search');
            }
            if(!$('#search-error').hasClass('d-none'))
            {
                $('#search-error').addClass('d-none');
            }
            
            $('#result_content').html(`
                <div class="spinner-grow text-success" role="status">
                    <span class="sr-only">Loading...</span>
                </div>
            `);
            $.ajax({
                type: "POST",
                url: "../busqueda-usuario.php",
                data: {
                    to_search
                },
                success: function (data) {
                    //console.log(data);
                    let json_data = JSON.parse(data);
                    $('#result_content').html('');
                    if (json_data.length > 0) {
                        $('#result_content').html(`
                        <div class="table-responsive mt-4">
                            <table class="table table-hover table-bordered" width="100%" cellspacing="0">
                                <thead class="text-center">
                                    <tr class="bg-green-strong text-white">
                                        <th>Matricula</th>
                                        <th>Nombre</th>
                                        <th>Tipo de usuario</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center" id="table_content">
                                </tbody >
                            </table>
                        </div>`)
                        json_data.forEach( element => {
                            table_td = `
                            <tr>
                                <td class="d-none">${element.id_persona}</td>
                                <td>${element.identificador}</td>
                                <td>${element.nombre}</td>
                                <td>`;
                            if(element.tipo_persona == 1 )
                            {
                                table_td = table_td + 'Admin';
                            }
                            else if (element.tipo_persona == 2) {
                                table_td = table_td + 'Empleado';
                            }
                            else if(element.tipo_persona == 3)
                            {
                                table_td = table_td + 'Profesor';
                            }
                            else if(element.tipo_persona == 4)
                            {
                                table_td = table_td + 'Alumno';
                            }
                            else
                            {
                                table_td = table_td + 'Externo';
                            }
                            table_td = table_td +
                            `
                            </td>
                                <td>
                                    <button class="btn btn-green-light-invert hacer"><ion-icon name="walk-outline" style="font-size: 20px; vertical-align: middle;"></ion-icon>&nbsp; REGISTRAR</button>
                                </td>
                            </tr>`
                            $('#table_content').append(table_td);
                        });
                    }
                    else {
                        $('#result_content').html(`
                        <div class="row no-gutters align-items-center">
                            <div class="col-auto m-0 p-0">
                                <img src="../img/svg/404_page_not_found_.svg" width="210"  alt="Usuarios no encontrados"></img>
                            </div>
                            <div class="col m-0 p-0">    
                                <p class="not-found-title">!No se encontraron usuarios con ese nombre o esa matricula!</p>
                                <p>Intenta buscar con una entrada diferente.</p>
                            </div>
                        </div>
                        `);
                    }
                }
            });
        }
        else {
            $('#to_search').removeClass('to_search');
            $('#to_search').addClass('invalid');
            $('#search-error').removeClass('d-none');
            $('#to_search').focus();
        }
    });

    $('#register-btn').click( function(event) {
        event.preventDefault();
        let go = true;
        let id_cp = $('#cp-externo').val();
        let externo_nombre = $('#externo-nombre').val();
        if(externo_nombre == ''){
            go = false;
        }
        let externo_email = $('#externo-email').val();
        if(externo_email == ''){
            go = false;
        }
        let externo_tel = $('#externo-tel').val();
        if(externo_tel == ''){
            go = false;
        }
        let externo_motivo = $('#externo-motivo').val();
        if(externo_motivo == ''){
            go = false;
        }
        if(go){
            $.ajax({
                type: "POST",
                url: "../registro-usuario.php",
                data: {
                    tipo_registro: "4",
                    externo_nombre,
                    id_cp,
                    externo_email,
                    externo_tel,
                    externo_motivo
                },
                success: function (data) {
                    let json_data = JSON.parse(data);
                    if (json_data.status == 202 ) {
                        toastr["success"](json_data.message);
                        $('#externo-nombre').val('');
                        $('#externo-email').val('');
                        $('#externo-tel').val('');
                        $('#externo-motivo').val('');
                        $('#externo-nombre').focus();
                    }
                    else if (json_data.status == 404 ) {
                        toastr["error"](json_data.message);
                    }
                }
            })
        }
        else{
            console.log('Falta rellenar datos');
        }
    });

    $('#result_content').on('click','.hacer', function(event) {
        event.preventDefault();
        let id_cp = $('#cp-interno').val();
        if(id_cp != 0) {
            let id_persona = $(this).parent().parent().children()[0].innerHTML;
            $.ajax({
                type: "POST",
                url: "../registro-usuario.php",
                data: {
                    tipo_registro: "3", // Registro manual de persona ya registrada
                    id_persona,
                    id_cp 
                },
                success: function (data) {
                    let json_data = JSON.parse(data);
                    if (json_data.status == 202 ) {
                        toastr["success"](json_data.message);
                        $('#result_content').html('');
                        $('#to_search').val('');
                        $('#to_search').focus();
                    }
                    else if (json_data.status == 404 ) {
                        toastr["error"](json_data.message);
                    }
                }
            })
        }
        else {
            $('#cp-interno').removeClass('cp-interno');
            $('#cp-interno').addClass('invalid');
            $('#select-error').removeClass('d-none');
            $('#cp-interno').focus();
        }
    });

    $('#cp-interno').change( function(event) {

        if($('#cp-interno').hasClass('invalid')) {
            $('#cp-interno').removeClass('invalid');
            $('#cp-interno').addClass('cp-interno');
        }
        if(!$('#select-error').hasClass('d-none')) {
            $('#select-error').addClass('d-none');
        }
    });
});

toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-bottom-left",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "7000",
    "hideDuration": "1000",
    "timeOut": "7000",
    "extendedTimeOut": "5000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}