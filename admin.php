<?php
include('database.php');
session_start();

date_default_timezone_set('America/Tegucigalpa');
$date_now = date('Y-m-d');
$records = $connection->prepare('SELECT COUNT(id_acceso) AS registros_actuales FROM acceso WHERE registered_at >= :fecha_actual;');
$records->bindParam('fecha_actual',$date_now);
$records->execute();
$result = $records->fetch(PDO::FETCH_ASSOC);

$records = $connection->prepare('SELECT COUNT(id_acceso) AS profesores_actuales FROM acceso A, persona P WHERE A.id_persona = P.id_persona AND P.tipo_persona=3 AND registered_at >= :fecha_actual;');
$records->bindParam('fecha_actual',$date_now);
$records->execute();
$profesor = $records->fetch(PDO::FETCH_ASSOC);

$records = $connection->prepare('SELECT COUNT(id_acceso) AS alumnos_actuales FROM acceso A, persona P WHERE A.id_persona = P.id_persona AND P.tipo_persona=4 AND registered_at >= :fecha_actual;');
$records->bindParam('fecha_actual',$date_now);
$records->execute();
$alumno = $records->fetch(PDO::FETCH_ASSOC);

$records = $connection->prepare('SELECT COUNT(id_acceso) AS invitados_actuales FROM acceso A, persona P WHERE A.id_persona = P.id_persona AND P.tipo_persona=5 AND registered_at >= :fecha_actual;');
$records->bindParam('fecha_actual',$date_now);
$records->execute();
$invitado = $records->fetch(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Admin - Sistema de control de acceso FI UAEM</title>
  <!-- Templates and librarys-->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
  <link href="fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link rel="icon" type="image/png" href="favicon.png" />
  <!-- Custom styles for this template-->
  <link href="css/standard-style.css" rel="stylesheet">
  <link rel="stylesheet" href="css/admin-style.css">
</head>
<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    <!-- Sidebar -->
    <ul class="navbar-nav sidebar accordion" id="accordionSidebar">
      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="">
        <div class="sidebar-brand-icon">
          <img src="img/png/uaem-logo.png" alt="Universidad Autonoma del Estado de Mexico" width="57px" height="50px" style="border-radius:3px;">
        </div>
        <div class="sidebar-brand-text mx-2">UAEM</div>
      </a>
      <!-- Divider -->
      <hr class="sidebar-divider my-0">
      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="admin.php">
          <ion-icon name="home-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
          <span>Inicio</span>
        </a>
      </li>
      <!-- Divider -->
      <hr class="sidebar-divider">
      <!-- Heading -->
      <div class="sidebar-heading">
        Operaciones
      </div>
      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <ion-icon name="people-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
          <span>Usuarios</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-light py-2 collapse-inner rounded">
            <h6 class="collapse-header">Tipo de usuario:</h6>
            <a class="collapse-item" href="admin-files/alumno_data.php">Alumnos</a>
            <a class="collapse-item" href="admin-files/profesor_data.php">Profesores</a>
            <a class="collapse-item" href="admin-files/trabajador_data.php" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;">Encargados de puntos de control</a>
            <a class="collapse-item" href="admin-files/empleado_data.php">Otros empleados</a>
            <a class="collapse-item" href="admin-files/invitado_data.php">Usuarios externos</a>
            <a class="collapse-item" href="admin-files/admin_data.php">Administradores</a>
          </div>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="" data-toggle="collapse" data-target="#collapseCatalogos" aria-expanded="true" aria-controls="collapseCatalogos">
          <ion-icon name="browsers-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
          <span>Catálogos</span>
        </a>
        <div id="collapseCatalogos" class="collapse" aria-labelledby="headingCatalogos" data-parent="#accordionSidebar">
          <div class="bg-light py-2 collapse-inner rounded">
            <h6 class="collapse-header">Catálogos disponibles:</h6>
            <a class="collapse-item" href="">Grupos</a>
            <a class="collapse-item" href="admin-files/materia_data.php">Materias</a>
          </div>
        </div>
      </li>
      <!-- Nav Item - Tables -->
      <li class="nav-item">
        <a class="nav-link" href="admin-files/puntoc_data.php">
          <ion-icon name="contract-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
          <span>Puntos de control</span>
        </a>
      </li>
      <hr class="sidebar-divider">
      <div class="sidebar-heading">
        Búsquedas
      </div>
      <li class="nav-item">
        <a class="nav-link" href="admin-files/admin-busqueda.php">
          <ion-icon name="search-circle-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
          <span>Busqueda avanzada</span>
        </a>
      </li>
      <hr class="sidebar-divider">
      <div class="sidebar-heading">
        Estadísticas y reportes
      </div>
      <li class="nav-item">
        <a class="nav-link" href="tables.html">
          <ion-icon name="bar-chart-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
          <span>Estadísticas</span>
        </a>
      </li>
      <hr class="sidebar-divider">
      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>
    </ul>
    <!-- End of Sidebar -->
    
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">

      <?php include($_SERVER['DOCUMENT_ROOT'].'/sistemaAccesoFI/templates/navbar.php'); ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4">Inicio</h1>
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <p>Estadísticas para el día de hoy 22/12/2020</p>
            <!--
            <a href="" class="btn btn-hov d-none d-inline-block" style="font-size:15px;">
              <ion-icon name="eye-outline" style="font-size: 22px; vertical-align: middle;"></ion-icon> &nbsp; Ver estadísticas detalladas
            </a>
            -->
          </div>

          <!-- Content Row -->
          <div class="row">

            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card card-top card-hov shadow h-100" style="text-decoration:none">
                    <div class="card-body">
                      <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                          <div class="card-top-title">Accesos totales</div>
                          <div class="card-top-content"><?=$result['registros_actuales']?></div>
                        </div>
                        <div class="col-auto">
                          <ion-icon name="people-circle-outline" style="color: gray; font-size: 40px;"></ion-icon>
                        </div>
                      </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card card-top card-hov shadow h-100" style="text-decoration:none">
                    <div class="card-body">
                      <div class="row no-gutters align-items-center">
                        <div class="col-md-10">
                          <div class="card-top-title">Alumnos</div>
                          <div class="card-top-content"><?=$alumno['alumnos_actuales']?></div>
                        </div>
                        <div class="col-md-2">
                          <ion-icon name="people-circle-outline" style="color: gray; font-size: 40px;"></ion-icon>
                        </div>
                      </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card card-top card-hov shadow h-100" style="text-decoration:none">
                    <div class="card-body">
                      <div class="row no-gutters align-items-center">
                        <div class="col-md-10">
                          <div class="card-top-title">Profesores</div>
                          <div class="card-top-content"><?=$profesor['profesores_actuales']?></div>
                        </div>
                        <div class="col-md-2">
                          <ion-icon name="people-circle-outline" style="color: gray; font-size: 40px;"></ion-icon>
                        </div>
                      </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card card-top card-hov shadow h-100" style="text-decoration:none">
                    <div class="card-body">
                      <div class="row no-gutters align-items-center">
                        <div class="col-md-10">
                          <div class="card-top-title">Usuarios externos</div>
                          <div class="card-top-content"><?=$invitado['invitados_actuales']?></div>
                        </div>
                        <div class="col-md-2">
                          <ion-icon name="people-circle-outline" style="color: gray; font-size: 40px;"></ion-icon>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->
    </div>
    <!-- End of Content Wrapper -->
  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <?php include('templates/logout-modal.php'); ?>

  <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
  <script src="https://unpkg.com/ionicons@5.2.3/dist/ionicons.js"></script>
  <!-- Custom scripts-->
  <script src="js/standard-func.js"></script>
</body>
</html>