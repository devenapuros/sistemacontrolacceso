<?php
    include('../database.php');
    $id_cp = $_POST['id_cp'];
    $records = $connection->prepare('DELETE FROM punto_control WHERE id_cp = :id_cp');
    $records->bindParam('id_cp',$id_cp);
    if( $records->execute() ){
        $records = $connection->prepare('SELECT id_cp, descripcion FROM punto_control;');
        $records->execute();
        $cps = json_encode($records->fetchAll());
        $res = array(
            "status" => 202,
            "message" => "Punto de control eliminado!",
            "cps" => $cps
        );
        echo json_encode($res);
    } else {
        $res = array("status" => 404, "message" => 'No se puede eliminar este punto de control ya que sus datos estan siendo usados en otra parte del sistema.');
        echo json_encode($res);
    }
?>