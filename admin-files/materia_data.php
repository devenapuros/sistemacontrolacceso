<?php
include('../database.php');
session_start();
$records = $connection->prepare('SELECT id_materia, nombre FROM materia;');
$records->execute();
$materias = $records->fetchAll();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Admin - Sistema de control de acceso FI UAEM</title>
    <!-- Custom fonts for this template-->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
    <link href="../fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link rel="icon" type="image/png" href="../favicon.png">
    <!-- Custom styles for this page -->
    <link href="../js/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link rel="stylesheet" href="../toastr/build/toastr.css">
    <link href="../css/standard-style.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/admin-style.css">
</head>
<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
        <!-- Sidebar -->
        <ul class="navbar-nav sidebar accordion" id="accordionSidebar">
            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="">
                <div class="sidebar-brand-icon">
                    <img src="/sistemaAccesoFI/img/png/uaem-logo.png" alt="" width="57px" height="50px" style="border-radius:3px;">
                </div>
                <div class="sidebar-brand-text mx-2">UAEM</div>
            </a>
            <!-- Divider -->
            <hr class="sidebar-divider my-0">
            <!-- Nav Item - Dashboard -->
            <li class="nav-item">
                <a class="nav-link" href="../admin.php">
                    <ion-icon name="home-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
                    <span>Inicio</span>
                </a>
            </li>
            <!-- Divider -->
            <hr class="sidebar-divider">
            <!-- Heading -->
            <div class="sidebar-heading">
                Operaciones
            </div>
            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                    <ion-icon name="people-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
                    <span>Usuarios</span>
                </a>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-light py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Tipo de usuario:</h6>
                        <a class="collapse-item" href="alumno_data.php">Alumnos</a>
                        <a class="collapse-item" href="profesor_data.php">Profesores</a>
                        <a class="collapse-item" href="trabajador_data.php" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;">Encargados de puntos de control</a>
                        <a class="collapse-item" href="empleado_data.php">Otros empleados</a>
                        <a class="collapse-item" href="invitado_data.php">Usuarios externos</a>
                        <a class="collapse-item" href="admin_data.php">Administradores</a>
                    </div>
                </div>
            </li>
            <li class="nav-item active">
                <a class="nav-link collapsed" href="" data-toggle="collapse" data-target="#collapseCatalogos" aria-expanded="true" aria-controls="collapseCatalogos">
                    <ion-icon name="browsers-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
                    <span>Catálogos</span>
                </a>
                <div id="collapseCatalogos" class="collapse" aria-labelledby="headingCatalogos" data-parent="#accordionSidebar">
                    <div class="bg-light py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Catálogos disponibles:</h6>
                        <a class="collapse-item" href="grupo_data.php">Grupos</a>
                        <a class="collapse-item active" href="materia_data.php">Materias</a>
                    </div>
                </div>
            </li>
            <!-- Nav Item - Tables -->
            <li class="nav-item">
                <a class="nav-link" href="puntoc_data.php">
                    <ion-icon name="contract-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
                    <span>Puntos de control</span>
                </a>
            </li>
            <!-- Divider -->
            <hr class="sidebar-divider">
            <!-- Heading -->
            <div class="sidebar-heading">
                Búsquedas
            </div>
            <!-- Nav Item - Tables -->
            <li class="nav-item">
                <a class="nav-link" href="tables.html">
                    <ion-icon name="search-circle-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
                    <span>Busqueda avanzada</span>
                </a>
            </li>
            <!-- Divider -->
            <hr class="sidebar-divider">
            <!-- Heading -->
            <div class="sidebar-heading">
                Estadísticas y reportes
            </div>

            <!-- Nav Item - Tables -->
            <li class="nav-item">
                <a class="nav-link" href="tables.html">
                    <ion-icon name="bar-chart-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
                    <span>Estadísticas</span>
                </a>
            </li>
            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>
        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">

                <?php include($_SERVER['DOCUMENT_ROOT'].'/sistemaAccesoFI/templates/navbar.php'); ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <nav aria-label="breadcrumb" style="font-size: 14px">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../admin.php" style="color: #958419;">Inicio</a></li>
                            <li class="breadcrumb-item" style="color: #958419;">Catálogos</li>
                            <li class="breadcrumb-item active" aria-current="page"><a href="">Materias</a></li>
                        </ol>
                    </nav>

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 mt-4 text-gray-800">Datos de materias</h1>
                    <p class="mb-4">A continuación se muestran la informacion almacenada de todas los materias.</p>

                    <!--BOTON PARA UN NUEVO REGISTRO-->
                    <div class="container">
                        <div class="row my-3">
                            <button class="btn btn-hov ml-auto" type="submit" data-toggle="modal" data-target="#newModal">
                                <ion-icon name="library-outline" style="font-size: 22px; vertical-align: middle;"></ion-icon>
                                &nbsp; Nuevo materia
                            </button>
                            <button class="btn btn-green-light ml-2" type="submit">
                                <ion-icon name="duplicate-outline" style="font-size: 22px; vertical-align: middle;"></ion-icon>
                                &nbsp; Cargar CSV
                            </button>
                        </div>
                    </div>
                    <!--TERMINA BOTON DE NUEVO REGISTRO-->

                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold" syle="color:#1C3D14">Materias disponibles</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover table-condensed" id="dataTable" style="width:100%">
                                    <thead class="text-center">
                                        <tr class="bg-green-strong text-white">
                                            <th>Clave</th>
                                            <th>Nombre</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tfoot class="text-center">
                                        <tr class="bg-green-strong text-white">
                                            <th>Clave</th>
                                            <th>Nombre</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </tfoot>
                                    <tbody style="background-color: white" class="text-center" id="table-body">
                                        <?php for ($i = 0; $i < sizeof($materias); $i++) { ?>
                                            <tr>
                                                <td><?= $materias[$i]['id_materia'] ?></td>
                                                <td><?= $materias[$i]['nombre'] ?></td>
                                                <td class="">
                                                    <button type="button" class="btn btn-outline-dark open-edit-modal ml-auto mr-1">
                                                        <ion-icon name="list-outline" style="font-size: 22px; vertical-align: middle;"></ion-icon>
                                                    </button>
                                                    <button type="button" class="btn btn-outline-danger open-delete-modal mr-auto ml-1">
                                                        <ion-icon name="trash-outline" style="font-size: 22px; vertical-align: middle;"></ion-icon>
                                                    </button>
                                                </td>
                                            </tr>
                                        <?php }; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- End of Main Content -->
        </div>
        <!-- End of Content Wrapper -->
    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

     <!--MODAL PARA EDITAR FORMULARIO-->
    <div class="modal fade" id="editModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Editar datos de materia</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                 </div>
                <div class="modal-body">
                    <form class="">
                        <div class="modal-body">
                            <div class="row bg-warning py-1 mb-3 align-items-center" style="border-radius:4px">
                                <div class="col-auto">
                                    <ion-icon name="warning-outline" style="font-size: 27px; vertical-align: middle;"></ion-icon>
                                </div>
                                <div class="col" style="text-align:center">
                                    Edita los campos siguientes solo si sabes lo que haces
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Clave de la materia:</label>
                                <input class="form-control" type="text" name="edit_id_materia" id="edit_id_materia" disabled>
                            </div>
                            <div class="form-group">
                                <label for="">Nombre:</label>
                                <input class="form-control" type="text" name="edit_materia_nombre" id="edit_materia_nombre">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-dark" type="cancel" data-dismiss="modal" id="cancel-btn">Cancelar</button>
                    <button class="btn btn-hov" type="button" id="edit-btn">Guardar</button>
                </div>
            </div>
        </div>
    </div>
    <!--FIN FORMULARIO EDITAR MODAL-->

    <!--MODAL PARA CREAR UN NUEVO REGISTRO-->
    <div class="modal fade" id="newModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Crear materia nueva</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                         <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="">
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="">Clave de la materia:</label>
                                <input class="form-control" type="text" name="new_id_materia" id="new_id_materia" placeholder="Ingrese clave de la materia">
                            </div>
                            <div class="form-group">
                                <label for="">Nombre:</label>
                                <input class="form-control" type="text" name="new_materia_nombre" id="new_materia_nombre" placeholder="Ingrese nombre de la materia">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-dark" type="cancel" data-dismiss="modal" id="cancel-btn">Cancelar</button>
                    <button class="btn btn-hov" type="submit" id="create-btn">Crear</button>
                </div>
            </div>
        </div>
    </div>
    <!--FIN FORMULARIO MODAL-->

    <!-- delete Modal-->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="delete-modal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModal">Seguro que deseas borrar este materia?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="bg-warning py-3 my-2" style="text-align:center; border-radius:4px">
                        <ion-icon name="warning-outline" style="font-size: 27px; vertical-align: middle;"></ion-icon>
                        &nbsp; Recuerda que esta acción no se puede deshacer
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-dark" type="cancel" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-danger" id="delete-btn">Eliminar</button>
                </div>
            </div>
        </div>
    </div>
    
    <?php include('../templates/logout-modal.php'); ?>

    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/ionicons@5.2.3/dist/ionicons.js"></script>
    <!-- Page level plugins -->
    <script src="../js/datatables/jquery.dataTables.min.js"></script>
    <script src="../js/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Custom scripts-->
    <script src="../toastr/build/toastr.min.js"></script>
    <script src="../js/standard-func.js"></script>
    <script src="../js/admin/materia-func.js"></script>
</body>
</html>