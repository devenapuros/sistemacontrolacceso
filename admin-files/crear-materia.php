<?php
    include('../database.php');
    
    $id_materia = $_POST['id_materia'];
    $nombre = $_POST['nombre'];

    $records = $connection->prepare('INSERT INTO materia (id_materia,nombre) VALUES (:id_materia,:nombre);');
    $records->bindParam('id_materia',$id_materia);
    $records->bindParam('nombre',$nombre);
    if($records->execute()) {
        $res = array("status" => 202, "message" => 'Materia creada exitosamente!');
        echo json_encode($res);
    }
    else{
        $res = array("status" => 404, "message" => 'No se pudo realizar el registro. Parece que el servidor esta tenido problemas. Intenta realizar la operacion mas tarde');
        echo json_encode($res);
    }
?>