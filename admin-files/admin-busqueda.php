<?php
  include('../database.php');
  session_start();
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Admin - Sistema de control de acceso FI UAEM</title>
  <!-- Custom fonts for this template-->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
  <link href="../fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link rel="icon" type="image/png" href="../favicon.png"/>
  <!-- Custom styles for this template-->
  <link href="../css/standard-style.css" rel="stylesheet">
  <link rel="stylesheet" href="../css/busqueda-style.css">
</head>
<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    <!-- Sidebar -->
    <ul class="navbar-nav sidebar accordion" id="accordionSidebar">
      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="">
        <div class="sidebar-brand-icon">
          <img src="/sistemaAccesoFI/img/png/uaem-logo.png" alt="" width="57px" height="50px" style="border-radius:3px;">
        </div>
        <div class="sidebar-brand-text mx-2">UAEM</div>
      </a>
      <!-- Divider -->
      <hr class="sidebar-divider my-0">
      <!-- Nav Item - Dashboard -->
      <li class="nav-item">
        <a class="nav-link" href="../admin.php">
          <ion-icon name="home-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
          <span>Inicio</span></a>
      </li>
      <!-- Divider -->
      <hr class="sidebar-divider">
      <!-- Heading -->
      <div class="sidebar-heading">
        Operaciones
      </div>
      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
                <a class="nav-link collapsed" href="" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                    <ion-icon name="people-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
                    <span>Usuarios</span>
                </a>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-light py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Tipo de usuario:</h6>
                        <a class="collapse-item" href="alumno_data.php">Alumnos</a>
                        <a class="collapse-item" href="profesor_data.php">Profesores</a>
                        <a class="collapse-item" href="trabajador_data.php" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;">Encargados de puntos de control</a>
                        <a class="collapse-item" href="empleado_data.php">Otros empleados</a>
                        <a class="collapse-item" href="invitado_data.php">Usuarios externos</a>
                        <a class="collapse-item" href="admin_data.php">Administradores</a>
                    </div>
                </div>
            </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="" data-toggle="collapse" data-target="#collapseCatalogos" aria-expanded="true" aria-controls="collapseCatalogos">
          <ion-icon name="browsers-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
          <span>Catálogos</span>
        </a>
        <div id="collapseCatalogos" class="collapse" aria-labelledby="headingCatalogos" data-parent="#accordionSidebar">
          <div class="bg-light py-2 collapse-inner rounded">
            <h6 class="collapse-header">Catálogos disponibles:</h6>
            <a class="collapse-item" href="">Grupos</a>
            <a class="collapse-item" href="materia_data.php">Materias</a>
          </div>
        </div>
      </li>

      <!-- Nav Item - Tables -->
      <li class="nav-item">
        <a class="nav-link" href="puntoc_data.php">
          <ion-icon name="contract-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
          <span>Puntos de control</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">
      <!-- Heading -->
      <div class="sidebar-heading">
        Búsquedas
      </div>

      <!-- Nav Item - Tables -->
      <li class="nav-item active">
        <a class="nav-link" href="admin-busqueda.php">
          <ion-icon name="search-circle-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
          <span>Busqueda avanzada</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">
      <!-- Heading -->
      <div class="sidebar-heading">
        Estadísticas y reportes
      </div>

      <!-- Nav Item - Tables -->
      <li class="nav-item">
        <a class="nav-link" href="">
          <ion-icon name="bar-chart-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
          <span>Estadísticas</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">

        <?php include($_SERVER['DOCUMENT_ROOT'].'/sistemaAccesoFI/templates/navbar.php'); ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

            <!-- Page Heading -->
          <h1 class="h3 mb-4">Búsqueda avanzada</h1>
          <p class="mb-4">En esta sección puedes buscar a personas por su nombre o su numero de matrícula y podras ver si han pasado por alguno de los puntos de control disponibles. Ademas puedes hacer busquedas por hora o por fecha de registro.</p>
          
          <div class="row">
            <div class="col-md-12 mx-auto">
              <div class="card card-top shadow h-100">
                    <div class="card-body">
                      <form class="mt-3">
                        <div class="input-group mb-2">
                          <div class="mr-1">
                            <button class="btn btn-hov-invert" type="button" data-toggle="modal" data-target="#filterModal">
                              <ion-icon name="funnel-outline" style="font-size: 20px; vertical-align: middle;"></ion-icon>
                              <span class="d-none d-sm-inline">&nbsp; Filtrar búsqueda</span>
                            </button>
                          </div>
                          <input type="text" class="form-control to_search" id="to_search" placeholder="Escribe aquí para buscar" autofocus>
                          <div class="input-group-append">
                            <button class="btn btn-hov btn-primary" type="submit">
                              <span class="d-none d-sm-inline">Buscar &nbsp;</span>
                              <ion-icon name="search-outline" style="font-size: 20px; vertical-align: middle;"></ion-icon>
                            </button>
                          </div>
                        </div>
                      </form>
                      <div id="search-error" class="error-text d-none">
                        Debes ingresar un nombre o una matrícula para buscar
                      </div>
                      <div class="row mt-3">
                        <div class="col-auto">
                        <p class="font-weight-bold">Filtros:</p>
                        </div>    
                        <div class="col-auto">
                          <div class="card py-1 flex-row d-flex align-items-center" style="background:#dfe6e9; border:none; color:#576574; font-size:15px">
                          &nbsp; Tipo de usuario &nbsp; &nbsp;
                          <button class="btn btn-link p-0 m-0 ml-auto">
                            <ion-icon class="font-weight-bold" style="color:gray;font-size:23px; vertical-align: middle; padding-right:5px;" name="close-outline"></ion-icon>
                          </button>
                          </div>
                        </div>

                      </div>
                    </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12 mx-auto">
              <!-- 
              <div class="card card-top shadow h-100">
                    <div class="card-body">
                    
                    </div>
              </div>
              -->
            </div>
          </div>
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->
    </div>
    <!-- End of Content Wrapper -->
  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <div class="modal fade" id="filterModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Filtrar busqueda por:</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body bg-light">
          <div class="container-fluid">

            <div class="row my-4">
              <div class="col py-3">
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                  <label class="form-check-label" for="flexCheckDefault">
                    Tipo de usuario:
                  </label>
                  <select class="form-control form-select" aria-label="form-check-label" for="flexCheckDefault" disabled>
                    <option selected>Alumno</option>
                    <option value="1">Profesor</option>
                    <option value="2">Administrativo</option>
                    <option value="3">Invitado</option>
                  </select>
                </div>
              </div>
            </div>

            <div class="row my-4">
              <div class="col py-3">
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" value="" id="defaultCheck">
                  <label class="form-check-label" for="defaultCheck">
                    Fecha de registro:
                  </label>
                  <input class="form-control" type="date" disabled>
                </div> 
              </div>
            </div>

            <div class="row my-4">
              <div class="col py-3 shadow card-top" style="background:white;border-radius:4px;">
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" value="" checked="checked" id="defaultCheck1">
                  <label class="form-check-label" for="defaultCheck1">
                    Hora de registro:
                  </label>
                  <input class="form-control" type="time">
                </div>
              </div> 
            </div>

          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
          <button type="button" class="btn btn-primary">Aplicar</button>
        </div>
      </div>
    </div>
  </div>

  <?php include('../templates/logout-modal.php'); ?>

  <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
  <script src="https://unpkg.com/ionicons@5.2.3/dist/ionicons.js"></script>
  <!-- Custom scripts-->
  <script src="../js/standard-func.js"></script>
</body>
</html>
