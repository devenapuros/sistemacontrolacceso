<?php
    include('../database.php');
    $id_persona = $_POST['id_persona'];
    $tipo_persona = $_POST['tipo_persona'];
    $records = $connection->prepare('DELETE FROM persona WHERE id_persona = :id_persona');
    $records->bindParam('id_persona',$id_persona);
    if($records->execute()){
        $records = $connection->prepare('SELECT id_persona,identificador,nombre,email FROM persona WHERE tipo_persona = :tipo_persona;');
        $records->bindParam('tipo_persona',$tipo_persona);
        $records->execute();
        $personas = json_encode($records->fetchAll());
        $res = array(
            "status" => 202,
            "message" => "Usuario eliminado!",
            "personas" => $personas
        );
        echo json_encode($res);
    }
    else {
        $res = array("status" => 404, "message" => 'No se puede eliminar a este usuario ya que sus datos estan siendo usados en otra parte del sistema.');
        echo json_encode($res);
    }
?>