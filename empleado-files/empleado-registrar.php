<?php
  include('../database.php');
  session_start();
  $records = $connection->prepare('SELECT * FROM  punto_control;');
  $records->execute();
  $control_points = $records->fetchAll();
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Sistema de control de acceso FI UAEM</title>
  <!-- Custom fonts for this template-->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
  <link href="../fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link rel="icon" type="image/png" href="../favicon.png"/>
  <link rel="stylesheet" href="../toastr/build/toastr.css">
  <!-- Custom styles for this template-->
  <link href="../css/standard-style.css" rel="stylesheet">
  <link rel="stylesheet" href="../css/empleado-style.css">
</head>
<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    <!-- Sidebar -->
    <ul class="navbar-nav sidebar accordion" id="accordionSidebar">
      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="">
        <div class="sidebar-brand-icon">
          <img src="/sistemaAccesoFI/img/png/uaem-logo.png" alt="" width="57px" height="50px" style="border-radius:3px;">
        </div>
        <div class="sidebar-brand-text mx-2">UAEM</div>
      </a>
      <!-- Divider -->
      <hr class="sidebar-divider my-0">
      <!-- Nav Item - Dashboard -->
      <li class="nav-item">
        <a class="nav-link" href="../empleado.php">
          <ion-icon name="contract-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
          <span>Puntos de control</span>
        </a>
      </li>
      <!-- Divider -->
      <hr class="sidebar-divider">
      <!-- Heading -->
      <div class="sidebar-heading">
        Operaciones
      </div>
      <!-- Nav Item - Tables -->
      <li class="nav-item active">
        <a class="nav-link" href="">
        <ion-icon name="person-add-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
          <span>Registrar acceso</span>
        </a>
      </li>
      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">
      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>
    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
      <?php include($_SERVER['DOCUMENT_ROOT'].'/sistemaAccesoFI/templates/navbar.php'); ?>
        <!-- Begin Page Content -->
        <div class="container-fluid">
           <!-- Page Heading -->
           <div class="row">
               <div class="col-md-11 mx-auto">
                <h1 class="h3 mb-4">Registrar acceso de persona</h1>
                   <div class="card card-content shadow mb-4">
                       <div class="card-body">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                              <a class="nav-link tab active" id="nav-interno" data-toggle="tab" href="#nav-interno-tab" role="tab" aria-controls="nav-interno-tab" aria-selected="true">Interno</a>
                              <a class="nav-link tab" id="nav-externo" data-toggle="tab" href="#nav-externo-tab" role="tab" aria-controls="nav-externo-tab" aria-selected="false">Externo</a>
                            </div>
                          </nav>
                          <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-interno-tab" role="tabpanel" aria-labelledby="nav-interno">
                                <form class="mt-4">
                                    <label for="">Punto de control:</label>
                                    <select name="" class="form-control cp-interno" id="cp-interno">
                                      <option value="0" hidden selected>Selecciona un punto de control</option>
                                      <?php for ($i = 0; $i < sizeof($control_points); $i++){?>
                                        <option value="<?= $control_points[$i]['id_cp']?>"><?= $control_points[$i]['descripcion']?></option>
                                      <?php };?>
                                    </select>
                                    <div id="select-error" class="error-text d-none">
                                      Debes seleccionar un punto de control
                                    </div>
                                    <label class="mt-3" for="exampleFormControlInput1">Buscar persona:</label>
                                    <div class="input-group">
                                      <input type="text" class="form-control to_search" id="to_search" placeholder="Escribe el nombre o la matrícula de la persona a buscar" autofocus>
                                      <div class="input-group-append">
                                        <button class="btn btn-hov btn-primary" id="search-btn" type="submit">
                                          <span class="d-none d-sm-inline">Buscar &nbsp;</span>
                                          <ion-icon name="search-outline" style="font-size: 20px; vertical-align: middle;"></ion-icon>
                                        </button>
                                      </div>
                                    </div>
                                    <div id="search-error" class="error-text d-none">
                                      Debes ingresar un nombre o una matrícula
                                    </div>
                                    <div class="d-flex justify-content-center" id="result_content" style="margin-top:20px;">  
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane fade" id="nav-externo-tab" role="tabpanel" aria-labelledby="nav-externo">
                                <form class="mt-4">
                                    <div class="form-row">
                                        <div class="col-md-6 mb-2">
                                            <label for="">Nombre:</label>
                                            <input class="form-control" type="text" name="externo-nombre" id="externo-nombre" placeholder="Ingrese nombre completo de la persona a registrar">
                                        </div>
                                        <div class="col-md-6 mb-2">
                                            <label for="">Punto de control:</label>
                                            <select name="cp-externo" id="cp-externo" class="form-control mb-3">
                                            <?php for ($i = 0; $i < sizeof($control_points); $i++){?>
                                              <option value="<?= $control_points[$i]['id_cp']?>"><?= $control_points[$i]['descripcion']?></option>
                                            <?php };?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label for="">Email:</label>
                                            <input class="form-control" type="email" name="externo-email" id="externo-email" placeholder="Ingrese un email de contacto">
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="">Numero de celular:</label>
                                            <input class="form-control" type="tel" id="externo-tel" placeholder="Ingrese numero de celular">
                                        </div>
                                    </div>
                                    <div class="form-row mb-4">
                                        <label for="">Motivo de visita:</label>
                                        <textarea class="form-control" id="externo-motivo" rows="3" placeholder="Ingrese una descripcion del motivo de la visita"></textarea>
                                    </div>
                                    <div class="form-row mb-2">
                                        <button class="btn btn-hov mx-auto" type="submit" id="register-btn" style="width: 100%;">REGISTRAR</button>
                                    </div>
                                </form>
                            </div>
                          </div>
                       </div>
                   </div>
               </div>
           </div>
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->
    </div>
    <!-- End of Content Wrapper -->
  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <?php include('../templates/logout-modal.php'); ?>

  <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
  <script src="https://unpkg.com/ionicons@5.2.3/dist/ionicons.js"></script>
  <script src="../toastr/build/toastr.min.js"></script>
  <!-- Custom scripts for all pages-->
  <script src="../js/standard-func.js"></script>
  <script src="../js/empleado-registrar.js"></script>
</body>
</html>
