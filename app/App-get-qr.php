<?php
    include('../vendor/autoload.php');
    include('../database.php');
    use Endroid\QrCode\QrCode;
    session_start();
    date_default_timezone_set('America/Tegucigalpa');
    $date_now = date('d-m-Y');
    $id_cp = $_GET['id_cp'];
    $records = $connection->prepare('SELECT qrcode FROM punto_control WHERE id_cp = :id_cp');
    $records->bindParam('id_cp',$id_cp);
    $records->execute();
    $cp_result = $records->fetch(PDO::FETCH_ASSOC);
    $qr_elements = explode('_',$cp_result['qrcode']);
    $fecha_qr = strtotime($qr_elements[1]);
    $fecha_actual = strtotime($date_now);
    if ($fecha_actual > $fecha_qr) {
        $qr_text = $id_cp.'_'.$date_now.'_'.$qr_elements[2];
        $qrCode = new QrCode($qr_text);
        $qrCode->setSize(300);
        $image = $qrCode->writeString();
        $imageData = base64_encode($image);
        $records = $connection->prepare('UPDATE punto_control SET qrcode = :qrcode WHERE id_cp = :id_cp;');
        $records->bindParam('qrcode',$qr_text);
        $records->bindParam('id_cp',$cp_result['id_cp']);
        $records->execute();
        $records = $connection->prepare('UPDATE punto_control SET img_text = :imageData WHERE id_cp = :id_cp;');
        $records->bindParam('imageData',$imageData);
        $records->bindParam('id_cp',$cp_result['id_cp']);
        $records->execute();
        $res = array("img_text" => $imageData );
        echo json_encode($res);
    }
    else{
        $records = $connection->prepare('SELECT img_text FROM punto_control WHERE id_cp = :id_cp;');
        $records->bindParam('id_cp',$id_cp);
        $records->execute();
        $data = $records->fetch(PDO::FETCH_ASSOC);
        $imageData = $data['img_text'];
        $res = array("img_text" => $imageData );
        echo json_encode($res);
    }
    /*
    for ($i=0; $i < sizeof($qr_desc); $i++) { 
        $qr_text.= $qr_desc[$i];
    }*/
?>

